<?php
namespace Home\Controller;

use Home\Model\TopicModel;
class TopicController extends HomeController
{
     public function index(){
         $this->setSiteTitle('专题列表-'.$this->site_title);
         $this->assign('_list',$this->lists(D('Topic')));
         $this->display();
     }
     public function detail(){
         $id = I('id');
         $where['tid'] = $id;
         $TopicModel = new TopicModel();
         $topicInfo = $TopicModel->info($id);
         $this->setSiteTitle($topicInfo['topic_name']);
         $this->assign('goods',$this->lists(D('Goods'),$where));
         $this->assign('topic',$topicInfo);
         $this->display();
     }
}

?>