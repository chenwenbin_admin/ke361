<?php
namespace Admin\Controller;

use Admin\Model\AdModel;
class AdController extends AdminController
{
    public function index(){
        
        $list = $this->lists(D('Ad'));
        $this->assign('_list',$list);
        $this->display();
    }
    public function edit(){
        if(IS_POST){
            $ad['id']       =   I('post.id');
            $ad['title']    =   I('post.title');
            $ad['pic_url']  =   I('post.pic_url');
            $ad['link']     =   I('post.link');
            $AdModel        =   new AdModel();
            $res            =   $AdModel->addAd($ad);
            if($res){
                $this->success('添加成功',U('Ad/index'));
            }else {
                $this->error('添加失败');
            }
        }else{
            $id =   I('get.id');
            $AdModel = new AdModel();
            $this->assign('ad',$AdModel->info($id));
            $this->display();
        }
    }
    public function del(){
        
    }
}

?>